# Datuak gordetzea
[docs](https://docs.docker.com/storage/volumes/)

::: tip ARIKETA
Eguneratu `mysql`/`mariadb` edukiontzia.
:::

Datuak edukiontzi baten bizitza zikloari lotuta badaude... nola gorde eguneratze batetik bestera? Ala katastrofe baten ostean?

Docker-ek aplikazioen datuak kudeatzeko aukera ezberdinak ematen dizkigu:

* `bind mount`
* docker `volume`-ak
* `tmpfs`

## bind mount
[docs](https://docs.docker.com/storage/bind-mounts/)

Zerbitzariko fitxategi/direktorio bat muntatzean datza.

![bind mount type by docs.docker.com](./static/types-of-mounts-bind.png)

::: tip ARIKETA
Martxan jarri datubasea berriro, baina oraingoan `bind mount` batekin. Ezabatu edukiontzia eta berriro martxan jarri, datuak hor daudela konfirmatu :)
:::

## tmpfs

**Behin behineko** (memorian idazten den) fitxategi sistema. **Edukiontzia gelditzean ezabatzen da**.

![tmpfs mount type by docs.docker.com](./static/types-of-mounts-tmpfs.png)

### limitazioak

* Ezin dira edukiontzien artean partekatu
* Linux-en bakarrik

## docker volume-ak

[docs](https://docs.docker.com/storage/volumes/)

Docker-ek kudeatzen dituen *volume/karpeta/diska/fitxategi sistemak*.

Hainbat abantaila dituzte:

(TODO translate)

* Volumes are easier to back up or migrate than bind mounts.
* You can manage volumes using Docker CLI commands or the Docker API.
* Volumes work on both Linux and Windows containers.
* Volumes can be more safely shared among multiple containers.
* Volume drivers let you store volumes on remote hosts or cloud providers, to encrypt the * contents of volumes, or to add other functionality.
* New volumes can have their content pre-populated by a container.




![volume mount type by docs.docker.com](./static/types-of-mounts-volume.png)
